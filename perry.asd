;;;
;;; perry.asd
;;;
;;; ASDF system definition for perry.
;;;

(asdf:defsystem :perry
  :components
  ((:file "fileutils")
   (:file "package" :depends-on ("fileutils"))
   (:file "alcor-2" :depends-on ("package"))
   (:file "disassembler" :depends-on ("package"))
   (:file "ev5-cpu" :depends-on ("package" "alcor-2" "disassembler"))
   (:file "cpu-instructions" :depends-on ("ev5-cpu"))))

;;; EOF
