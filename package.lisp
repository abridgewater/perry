;;;
;;; package.lisp
;;;
;;; Package definitions for perry.
;;;

(cl:defpackage :perry
  (:use :cl :file-utils))

(cl:defpackage :alcor-2
  (:use :cl)
  (:export
   "INIT-ALCOR-2"

   "CPU-READ-LONG"
   "CPU-WRITE-LONG"
   "CPU-READ-QUAD"
   "CPU-WRITE-QUAD"))

;;; EOF
